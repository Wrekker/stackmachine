package se.frostbyte.jenga.error;

public class Message {
    public static void error(int line, String msg) {
        System.err.println("Line " + line + " - " + msg);
    }
}
