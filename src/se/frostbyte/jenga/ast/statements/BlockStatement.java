package se.frostbyte.jenga.ast.statements;

import se.frostbyte.jenga.ast.AstNode;
import se.frostbyte.jenga.ast.NodeType;

public class BlockStatement extends AstNode {
    public final AstNode[] nodes;

    public BlockStatement(AstNode... nodes) {
        super(NodeType.BlockStatement);
        this.nodes = nodes;
    }
}


