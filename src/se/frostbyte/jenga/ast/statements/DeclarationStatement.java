package se.frostbyte.jenga.ast.statements;

import se.frostbyte.jenga.ast.AstNode;
import se.frostbyte.jenga.ast.NodeType;
import se.frostbyte.jenga.ast.expressions.Expression;
import se.frostbyte.jenga.ast.expressions.VariableExpression;

public class DeclarationStatement extends AstNode {
    public final VariableExpression variable;
    public final Expression initializer;

    public DeclarationStatement(VariableExpression variable, Expression initializer) {
        super(NodeType.DeclarationStatement);
        this.variable = variable;
        this.initializer = initializer;
    }
}
