package se.frostbyte.jenga.ast.statements;

import se.frostbyte.jenga.ast.AstNode;
import se.frostbyte.jenga.ast.NodeType;
import se.frostbyte.jenga.ast.expressions.Expression;
import se.frostbyte.jenga.ast.expressions.VariableExpression;

public class AssignStatement extends AstNode {
    public final VariableExpression left;
    public final Expression right;

    public AssignStatement(VariableExpression left, Expression right) {
        super(NodeType.AssignStatement);
        this.left = left;
        this.right = right;
    }
}
