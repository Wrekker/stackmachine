package se.frostbyte.jenga.ast.statements;

import se.frostbyte.jenga.ast.AstNode;
import se.frostbyte.jenga.ast.NodeType;
import se.frostbyte.jenga.ast.expressions.Expression;

public class ForStatement extends AstNode {

	public final BlockStatement block;
	public final Expression condition;
	public final AstNode operations;
	public final AstNode body;

	public ForStatement(BlockStatement block, Expression condition, AstNode operations, AstNode body) {
		super(NodeType.ForStatement);
		this.block = block;
		this.condition = condition;
		this.operations = operations;
		this.body = body;
	}
}
