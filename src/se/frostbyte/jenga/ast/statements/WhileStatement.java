package se.frostbyte.jenga.ast.statements;

import se.frostbyte.jenga.ast.AstNode;
import se.frostbyte.jenga.ast.NodeType;
import se.frostbyte.jenga.ast.expressions.Expression;

public class WhileStatement extends AstNode {
    public final Expression condition;
    public final AstNode body;

    public WhileStatement(Expression condition, AstNode body) {
        super(NodeType.WhileStatement);
        this.condition = condition;
        this.body = body;
    }

    public String toString() {
        return "while (" + this.condition + ")" + this.body;
    }
}
