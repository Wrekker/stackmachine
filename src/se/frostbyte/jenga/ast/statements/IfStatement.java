package se.frostbyte.jenga.ast.statements;

import se.frostbyte.jenga.ast.AstNode;
import se.frostbyte.jenga.ast.NodeType;
import se.frostbyte.jenga.ast.expressions.Expression;

public class IfStatement extends AstNode {
    public final Expression condition;
	public final AstNode consequent;
    public final AstNode alternative;

	public IfStatement(Expression condition, AstNode consequent, AstNode alternative) {
        super(NodeType.IfStatement);
		this.condition = condition;
		this.consequent = consequent;
        this.alternative = alternative;
	}
}
