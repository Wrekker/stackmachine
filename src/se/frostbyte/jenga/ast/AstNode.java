package se.frostbyte.jenga.ast;

public abstract class AstNode {
    public final int type;

    protected AstNode(int nodeType) {
        type = nodeType;
    }
}
