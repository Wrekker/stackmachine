package se.frostbyte.jenga.ast.expressions;

public abstract class UnaryExpression extends Expression {
    public final Expression value;

    protected UnaryExpression(int nodeType, Expression value) {
        super(nodeType);
        this.value = value;
    }

    @Override
    public String toString() {
        return this.getClass().getName() +
                "[" + value.toString() + "]";
    }
}
