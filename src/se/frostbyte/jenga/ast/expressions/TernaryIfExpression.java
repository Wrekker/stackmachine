package se.frostbyte.jenga.ast.expressions;

import se.frostbyte.jenga.ast.NodeType;
import se.frostbyte.jenga.error.*;

public class TernaryIfExpression extends BinaryExpression {
    public TernaryIfExpression(Expression left, Expression right) {
        super(NodeType.TernaryIfExpression, left, right);

        if (!(right instanceof DecisionExpression)) {
            Message.error(-1, "Invalid use of ternary conditional.");
        }
    }
}
