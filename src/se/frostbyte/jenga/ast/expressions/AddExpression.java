package se.frostbyte.jenga.ast.expressions;

import se.frostbyte.jenga.ast.NodeType;

public class AddExpression extends BinaryExpression {
    public AddExpression(Expression left, Expression right) {
        super(NodeType.AddExpression, left, right);
    }
}
