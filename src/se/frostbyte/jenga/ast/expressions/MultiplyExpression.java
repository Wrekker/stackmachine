package se.frostbyte.jenga.ast.expressions;

import se.frostbyte.jenga.ast.NodeType;

public class MultiplyExpression extends BinaryExpression {
	public MultiplyExpression(Expression left, Expression right) {
		super(NodeType.MultiplyExpression, left, right);
	}
}
