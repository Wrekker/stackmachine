package se.frostbyte.jenga.ast.expressions;

import se.frostbyte.jenga.ast.NodeType;

public class ModuloExpression extends BinaryExpression {
	public ModuloExpression(Expression left, Expression right) {
		super(NodeType.ModuloExpression, left, right);
	}
}
