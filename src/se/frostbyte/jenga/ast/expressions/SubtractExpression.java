package se.frostbyte.jenga.ast.expressions;

import se.frostbyte.jenga.ast.NodeType;

public class SubtractExpression extends BinaryExpression {
    public SubtractExpression(Expression left, Expression right) {
        super(NodeType.SubtractExpression, left, right);
    }
}
