package se.frostbyte.jenga.ast.expressions;

public abstract class ScalarExpression extends Expression {
    protected ScalarExpression(int nodeType) {
        super(nodeType);
    }
}
