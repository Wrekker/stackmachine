package se.frostbyte.jenga.ast.expressions;

import se.frostbyte.jenga.ast.NodeType;

public class LessThanEqualExpression extends BinaryExpression {
	public LessThanEqualExpression(Expression left, Expression right) {
		super(NodeType.LesserEqualExpression, left, right);
	}
}
