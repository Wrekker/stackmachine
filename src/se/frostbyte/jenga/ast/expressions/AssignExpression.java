package se.frostbyte.jenga.ast.expressions;

import se.frostbyte.jenga.ast.NodeType;

public class AssignExpression extends BinaryExpression {
    public AssignExpression(Expression left, Expression right) {
        super(NodeType.AssignExpression, left, right);
    }
}
