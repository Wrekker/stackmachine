package se.frostbyte.jenga.ast.expressions;

import se.frostbyte.jenga.ast.NodeType;

public class DivideExpression extends BinaryExpression {
    public DivideExpression(Expression left, Expression right) {
        super(NodeType.DivideExpression, left, right);
    }
}
