package se.frostbyte.jenga.ast.expressions;

import se.frostbyte.jenga.ast.NodeType;

public class GreaterThanEqualExpression extends BinaryExpression {
	public GreaterThanEqualExpression(Expression left, Expression right) {
		super(NodeType.GreaterEqualExpression, left, right);
	}
}
