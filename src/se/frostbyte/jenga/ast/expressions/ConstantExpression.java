package se.frostbyte.jenga.ast.expressions;

import se.frostbyte.jenga.ast.NodeType;

public class ConstantExpression extends ScalarExpression {
    public final int value;

    public ConstantExpression(int value) {
        super(NodeType.ConstantExpression);
        this.value = value;
    }

    @Override
    public String toString() {
        return Integer.toString(this.value);
    }
}