package se.frostbyte.jenga.ast.expressions;

import se.frostbyte.jenga.ast.NodeType;

public class InequalityExpression extends BinaryExpression {
    public InequalityExpression(Expression left, Expression right) {
        super(NodeType.InequalityExpression, left, right);
    }
}
