package se.frostbyte.jenga.ast.expressions;

import se.frostbyte.jenga.ast.NodeType;

public class EqualityExpression extends BinaryExpression {
    public EqualityExpression(Expression left, Expression right) {
        super(NodeType.EqualityExpression, left, right);
    }
}
