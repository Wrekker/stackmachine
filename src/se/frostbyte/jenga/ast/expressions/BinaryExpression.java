package se.frostbyte.jenga.ast.expressions;

public abstract class BinaryExpression extends Expression {
    public final Expression left;
    public final Expression right;

    protected BinaryExpression(int nodeType, Expression left, Expression right) {
        super(nodeType);
        this.left = left;
        this.right = right;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() +
                "[" + left.toString() + ", " + right.toString() + "]";
    }
}
