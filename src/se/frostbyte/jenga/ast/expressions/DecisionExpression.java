package se.frostbyte.jenga.ast.expressions;

import se.frostbyte.jenga.ast.NodeType;

public class DecisionExpression extends BinaryExpression {
    public DecisionExpression(Expression left, Expression right) {
        super(NodeType.DecisionExpression, left, right);
    }
}
