package se.frostbyte.jenga.ast.expressions;

import se.frostbyte.jenga.ast.NodeType;

public class NegateExpression extends UnaryExpression {
    public NegateExpression(Expression value) {
        super(NodeType.NegateExpression, value);
    }
}
