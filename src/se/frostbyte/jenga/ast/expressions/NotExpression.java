package se.frostbyte.jenga.ast.expressions;

import se.frostbyte.jenga.ast.NodeType;

public class NotExpression extends UnaryExpression {
    public NotExpression(Expression value) {
        super(NodeType.NotExpression, value);
    }
}
