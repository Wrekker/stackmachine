package se.frostbyte.jenga.ast.expressions;

import se.frostbyte.jenga.ast.NodeType;

public class GreaterThanExpression extends BinaryExpression {
	public GreaterThanExpression(Expression left, Expression right) {
		super(NodeType.GreaterExpression, left, right);
	}
}
