package se.frostbyte.jenga.ast.expressions;

import se.frostbyte.jenga.ast.NodeType;
import se.frostbyte.jenga.ast.Symbol;

public class VariableExpression extends ScalarExpression {
    public final Symbol Symbol;

    public VariableExpression(Symbol sym) {
        super(NodeType.VariableExpression);
        Symbol = sym;
    }

    @Override
    public String toString() {
        return "$" + Symbol.name;
    }
}
