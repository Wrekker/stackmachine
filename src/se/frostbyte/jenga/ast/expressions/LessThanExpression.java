package se.frostbyte.jenga.ast.expressions;

import se.frostbyte.jenga.ast.NodeType;

public class LessThanExpression extends BinaryExpression {
	public LessThanExpression(Expression left, Expression right) {
		super(NodeType.LesserExpression, left, right);
	}
}
