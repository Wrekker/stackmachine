package se.frostbyte.jenga.ast;

import java.util.HashMap;

public class Symbol {
    private static HashMap<String, Symbol> _pool = new HashMap<>();
    public final String name;

    private Symbol(String name) {
        this.name = name;
    }

    public static Symbol Create(String name) {
        Symbol s;
        if ((s = _pool.get(name)) == null) {
            s = new Symbol(name);
            _pool.put(name, s);
        }

        return s;
    }
}
