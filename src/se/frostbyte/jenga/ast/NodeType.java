package se.frostbyte.jenga.ast;

public class NodeType {
    public static final int
            AddExpression = 1,
            ConstantExpression = 2,
            DivideExpression = 3,
            EqualityExpression = 4,
            GreaterEqualExpression = 5,
            GreaterExpression = 6,
            InequalityExpression = 7,
            LesserEqualExpression = 8,
            LesserExpression = 9,
            ModuloExpression = 10,
            MultiplyExpression = 11,
            NegateExpression = 12,
            NotExpression = 13,
            SubtractExpression = 15,
            VariableExpression = 16,

            AssignStatement = 17,
            BlockStatement = 18,
            DeclarationStatement = 19,
            IfStatement = 20,
            WhileStatement = 21,

            TernaryIfExpression = 22,
            DecisionExpression = 23,

            AssignExpression = 24,

            Count = 25,
    		ForStatement = 26;
}
