package se.frostbyte.jenga.parsing.lexer;

public class Token {
    protected final int _id;
    protected final String _token;
    protected final int _start;

    public Token(int id, String token, int start) {
        _id = id;
        _token = token;
        _start = start;
    }

    protected Token(Token copy) {
        _id = copy._id;
        _token = copy._token;
        _start = copy._start;
    }

    public int getId() { return _id; }
    public String getAttribute() { return _token; }
    public int getStart() { return _start; }

    public boolean hasAttribute(String token) {
        return _token.equalsIgnoreCase(token);
    }

    @Override
    public String toString() {
        return _token;
    }
}
