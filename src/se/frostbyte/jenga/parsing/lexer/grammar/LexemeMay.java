package se.frostbyte.jenga.parsing.lexer.grammar;

import se.frostbyte.jenga.parsing.lexer.LexerReader;

public class LexemeMay extends LexemeNonterminal {
    private final LexemeNode _node;

    public LexemeMay(LexemeNode node) {
        _node = node;
    }

    @Override
    public Match match(LexerReader r) {
        Match next;

        if ((next = _node.match(r)).isMatch) {
            return next;
        }

        return new Match(true, 0);
    }
}
