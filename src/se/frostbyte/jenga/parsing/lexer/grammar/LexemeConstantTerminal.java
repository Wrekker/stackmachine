package se.frostbyte.jenga.parsing.lexer.grammar;

import se.frostbyte.jenga.parsing.lexer.LexerReader;

public class LexemeConstantTerminal extends LexemeNode {
    private final String _c;

    public LexemeConstantTerminal(String constant) {
        _c = constant;
    }

    @Override
    public Match match(LexerReader reader) {
        if (        reader.length() >= _c.length()
                &&  reader.subSequence(0, _c.length()).toString().equalsIgnoreCase(_c)) {
            reader.advance(_c.length());
            return new Match(true, _c.length());
        }

        return new Match(false, 0);
    }
}
