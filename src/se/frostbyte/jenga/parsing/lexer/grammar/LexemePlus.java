package se.frostbyte.jenga.parsing.lexer.grammar;

import se.frostbyte.jenga.parsing.lexer.LexerReader;

public class LexemePlus extends LexemeNonterminal {
    private final LexemeNode _node;

    public LexemePlus(LexemeNode node) {
        _node = node;
    }

    @Override
    public Match match(LexerReader r) {
        Match next;
        int ret = 0;

        if (!(next = _node.match(r)).isMatch) {
            return new Match(false, 0);
        }
        ret += next.length;

        while ((next = _node.match(r)).isMatch) {
            ret += next.length;
        }

        return new Match(true, ret);
    }
}
