package se.frostbyte.jenga.parsing.lexer.grammar;

import se.frostbyte.jenga.parsing.lexer.LexerReader;

public class LexemeOr extends LexemeNonterminal {
    private final LexemeNode[] _a;

    public LexemeOr(LexemeNode... a) {
        _a = a;
    }

    @Override
    public Match match(LexerReader r) {
        Match next;

        r.mark();
        for (int i = 0; i < _a.length; i++) {
            LexemeNode n = _a[i];

            if ((next = n.match(r)).isMatch) {
                r.discardMark();
                return next;
            }
        }

        r.recallMark();
        return new Match(false, 0);
    }
}
