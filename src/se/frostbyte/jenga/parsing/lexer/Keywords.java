package se.frostbyte.jenga.parsing.lexer;

public class Keywords {

    public final static String If = "if";
    public final static String Else = "else";
    public final static String Struct = "struct";
    public final static String While = "while";
    public final static String For = "for";
    public final static String Union = "union";
    public final static String Return = "return";
	public final static String Var = "var";


    public static String regex() {
        String[] str = new String[] {
                If, Else, While, For, Union, Return
        };

        StringBuilder sb = new StringBuilder();

        for (String s : str) {
            sb.append(s).append("|");
        }

        sb.setLength(sb.length() - 1);

        return sb.toString();
    }
}
