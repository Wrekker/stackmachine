package se.frostbyte.jenga.parsing.lexer;


import se.frostbyte.jenga.parsing.lexer.grammar.Match;
import se.frostbyte.jenga.error.*;

public class Lexer<T extends Token> {
    private final ITokenFactory<T> _tokenFactory;
    private final Lexeme[] _lexemes;
    private int _linebreakId = -1;
    private int _whitespaceId = -1;
    private int _commentLineId = -1;
    private int _commentOpenId = -1;
    private boolean _isLbWs = false;

    private int _commentCloseId = -1;
    private T _peek;
    private LexerReader _reader;
    private int _idx = 0;
    private int _lineNumber = 1;

    public Lexer(String text, ITokenFactory<T> tokenFactory, Lexeme[] lexemes) {
        _tokenFactory = tokenFactory;
        _lexemes = lexemes;
        _reader = new LexerReader(text);
    }

    public void setLinebreak(int lexeme) { _linebreakId = lexeme; };
    public void setIsLinebreakWhiteSpace(boolean b) { _isLbWs = b; };

    public void setWhitespace(int lexeme) { _whitespaceId = lexeme; }
    public void setCommentLine(int lexeme) { _commentLineId = lexeme; }
    public void setCommentBlock(int open, int close) { _commentOpenId = open; _commentCloseId = close; }

    public int getLineNumber() {
        return _lineNumber;
    }

    public T next() {
        T ret;

        if (_peek != null) {
            ret = _peek;
            _peek = null;
            return ret;
        }

        return consume();
    }

    public T peek() {
        if (_peek == null) {
            _peek = consume();
        }

        return _peek;
    }

    private T consume() {
        T ret;

        // As long as we end on a line break (or possible additional whitespace)
        // This can happen after we've skipped a comment
        while (true) {
            // ignore whitespace
            while ((ret = parseLexeme()) != null && (
                    ret.getId() == _whitespaceId || (_isLbWs && ret.getId() == _linebreakId))) {
                if (ret.getId() == _linebreakId) {
                    _lineNumber++;
                }
            }

            if (ret == null) {
                break;
            }

            if (ret.getId() == _commentLineId) {
                while ((ret = parseLexeme()).getId() != _linebreakId);
                _lineNumber++;
                continue;
            }

            if (ret.getId() == _commentOpenId) {
                while ((ret = parseLexeme()).getId() != _commentCloseId) {
                    if (ret.getId() == _linebreakId) {
                        _lineNumber++;
                    }
                }
                continue;
            }

            break;
        }

        return ret;
    }

    public boolean peek(int lexeme) {
        if (_peek == null) {
            _peek = next();
        }

        return _peek != null && _peek.getId() == lexeme;
    }

    public boolean accept(int lexeme) {
        if (peek() != null && peek().getId() == lexeme) {
            next();
            return true;
        }
        return false;
    }

    public boolean accept(int lexeme, String token) {
        if (peek() != null && peek().getId() == lexeme) {
            if (peek().hasAttribute(token)) {
                next();
                return true;
            }
        }

        return false;
    }

    public int acceptMany(int lexeme) {
        int count = 0;
        while (peek() != null && peek().getId() == lexeme) {
            next();
            count++;
        }

        return count;
    }

    public T expect(int lexeme, String attrib) {
        T p = peek();
        if (p != null && p.getId() == lexeme && p.hasAttribute(attrib)) {
            return next();
        }

        Message.error(getLineNumber(), "Expected symbol \"" + Lexemes.toString(lexeme) + "\", found \"" + (peek() == null ? -1 : Lexemes.toString(peek().getId())) + "\"");
        return null;
    }


    public T expect(int lexeme) {
        if (peek() != null && peek().getId() == lexeme) {
            return next();
        }

        Message.error(getLineNumber(), "Expected symbol \"" + Lexemes.toString(lexeme) + "\", found \"" + (peek() == null ? -1 : Lexemes.toString(peek().getId())) + "\"");
        return null;
    }

    private T parseLexeme() {
        if (_reader.length() == 0) {
            return null;
        }

        Match m;
        for (Lexeme lex : _lexemes) {
            _reader.mark();
            if (!(m = lex.getNode().match(_reader)).isMatch) {
                _reader.recallMark();
                continue;
            }
            int pos = _reader.discardMark();
            String tok = _reader.subSequenceRaw(pos, pos + m.length).toString();

            return _tokenFactory.create(lex.getSymbolId(), tok, pos);
        }

        // No matching pattern, fall back to scanning 1 char at a time

        String tok = String.valueOf(_reader.charAt(0));
        _reader.advance(1);
        return _tokenFactory.create(-1, tok, _reader.getPosition() - 1);
    }
}
