package se.frostbyte.jenga.parsing.lexer;

import java.util.LinkedList;

public class LexerReader implements CharSequence {
    private final StringBuffer _text;
    private LinkedList<Integer> _marks;
    private int _idx;

    public LexerReader(String text) {
        _marks = new LinkedList<Integer>();
        _text = new StringBuffer(text);
        _idx = 0;
    }

    public void mark() {
        _marks.push(_idx);
    }

    public int recallMark() {
        _idx = _marks.pop();
        return _idx;
    }

    public int discardMark() {
        return _marks.pop();
    }

    public void advance(int pos) {
        _idx += pos;
    }

    public int getPosition() {
        return _idx;
    }

    @Override
    public int length() {
        return _text.length() - _idx;
    }

    @Override
    public char charAt(int index) {
        return _text.charAt(_idx + index);
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        return _text.subSequence(_idx + start, _idx + end);
    }

    public CharSequence subSequenceRaw(int start, int end) {
        return _text.subSequence(start, end);
    }
}
