package se.frostbyte.jenga.parsing.lexer;

import se.frostbyte.jenga.parsing.lexer.grammar.LexemeNode;

public class Lexeme {
    private final LexemeNode _node;
    private final int _symbol;

    public Lexeme(LexemeNode node, int symbol) {
        _node = node;
        _symbol = symbol;
    }

    public int getSymbolId() { return _symbol; }
    public LexemeNode getNode() { return _node; }
}
