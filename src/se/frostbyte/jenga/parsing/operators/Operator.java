package se.frostbyte.jenga.parsing.operators;

import se.frostbyte.jenga.ast.expressions.Expression;

import java.util.Stack;

public abstract class Operator {
    public final OperatorDefinitions.OperatorDefinition definition;

    public Operator(OperatorDefinitions.OperatorDefinition definition) {
        this.definition = definition;
    }

    public abstract Expression createExpression(Stack<Expression> parameterStack);

    public static Operator nary(int tokenId, int opCount) {
        switch (opCount) {
            case 1: return unary(tokenId);
            case 2: return binary(tokenId);
            default: throw new RuntimeException("Invalid number of operands.");
        }
    }

    public static Operator unary(int tokenId) {
        return new UnaryOperator(OperatorDefinitions.unary.get(tokenId));
    }

    public static Operator binary(int tokenId) {
        return new BinaryOperator(OperatorDefinitions.binary.get(tokenId));
    }
}

