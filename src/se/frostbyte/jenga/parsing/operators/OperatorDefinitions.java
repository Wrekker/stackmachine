package se.frostbyte.jenga.parsing.operators;

import se.frostbyte.jenga.ast.expressions.*;
import se.frostbyte.jenga.parsing.lexer.Lexemes;

import java.util.HashMap;

public class OperatorDefinitions {
    public static final HashMap<Integer, OperatorDefinition> unary = new HashMap<>();
    public static final HashMap<Integer, OperatorDefinition> binary = new HashMap<>();

    static {
        // Arithmetic
        unary(Lexemes.Plus, 11, false, null);
        unary(Lexemes.Minus, 11, false, NegateExpression.class);
        binary(Lexemes.Multiply, 10, true, MultiplyExpression.class);
        binary(Lexemes.Divide, 10, true, DivideExpression.class);
        binary(Lexemes.Plus, 9, true, AddExpression.class);
        binary(Lexemes.Minus, 9, true, SubtractExpression.class);

        // Relational
        binary(Lexemes.LAngle, 8, true, LessThanExpression.class);
        binary(Lexemes.RAngle, 8, true, GreaterThanExpression.class);
        binary(Lexemes.LessThanEqual, 8, true, LessThanEqualExpression.class);
        binary(Lexemes.GreaterThanEqual, 8, true, GreaterThanEqualExpression.class);

        // Bitwise
        binary(Lexemes.Ampersand, 7, true, null);
        binary(Lexemes.Circumflex, 6, true, null);
        binary(Lexemes.Pipe, 5, true, null);

        // Logical
        unary(Lexemes.Exclaimation, 11, true, NotExpression.class);
        binary(Lexemes.And, 4, true, null);
        binary(Lexemes.Or, 3, true, null);
        binary(Lexemes.Equality, 2, true, EqualityExpression.class);
        binary(Lexemes.Inequality, 2, true, InequalityExpression.class);

        // Ternary pairs
        binary(Lexemes.Query, 1, true, TernaryIfExpression.class); // ? :
        binary(Lexemes.Colon, 1, false, DecisionExpression.class); // ? :

        // Assignment
        binary(Lexemes.Equals, 0, false, AssignExpression.class);
    }

    private static void unary(int tokenId, int precedence, boolean leftAssoc, Class<? extends Expression> cls) {
        unary.put(tokenId, new OperatorDefinition(tokenId, precedence, 1, leftAssoc, cls));
    }

    private static void binary(int tokenId, int precedence, boolean leftAssoc, Class<? extends Expression> cls) {
        binary.put(tokenId, new OperatorDefinition(tokenId, precedence, 2, leftAssoc, cls));
    }

    public static class OperatorDefinition {
        public final int id;
        public final int precedence;
        public final int ops;
        public final boolean leftAssoc;
        public final Class<? extends Expression> cls;

        private OperatorDefinition(int id, int precedence, int maxOps, boolean leftAssoc, Class<? extends Expression> cls) {
            this.id = id;
            this.precedence = precedence;
            this.ops = maxOps;
            this.leftAssoc = leftAssoc;
            this.cls = cls;
        }
    }
}
