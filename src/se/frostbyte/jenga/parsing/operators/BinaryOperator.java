package se.frostbyte.jenga.parsing.operators;

import se.frostbyte.jenga.ast.expressions.Expression;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Stack;

public class BinaryOperator extends Operator {
    public BinaryOperator(OperatorDefinitions.OperatorDefinition definition) {
        super(definition);
    }

    @Override
    public Expression createExpression(Stack<Expression> parameterStack) {
        Expression right = parameterStack.pop();

        try {
            Constructor<? extends Expression> constructor = definition.cls.getConstructor(Expression.class, Expression.class);
            return constructor.newInstance(parameterStack.pop(), right);
        } catch (NoSuchMethodException e) {
        } catch (InstantiationException e) {
        } catch (IllegalAccessException e) {
        } catch (InvocationTargetException e) { }

        return null;
    }
}
