package se.frostbyte.jenga.parsing.operators;

import se.frostbyte.jenga.ast.expressions.Expression;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Stack;

public class UnaryOperator extends Operator {
    public UnaryOperator(OperatorDefinitions.OperatorDefinition def) {
        super(def);
    }

    @Override
    public Expression createExpression(Stack<Expression> parameterStack) {
        Expression right = parameterStack.pop();

        try {
            Constructor<? extends Expression> constructor = definition.cls.getConstructor(Expression.class);
            return constructor.newInstance(parameterStack.pop(), right);
        }
        catch (NoSuchMethodException e) { throw new RuntimeException(e); }
        catch (InstantiationException e) { throw new RuntimeException(e); }
        catch (IllegalAccessException e) { throw new RuntimeException(e); }
        catch (InvocationTargetException e) { throw new RuntimeException(e); }
    }
}
