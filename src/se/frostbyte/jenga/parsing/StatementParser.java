package se.frostbyte.jenga.parsing;

import java.util.LinkedList;
import java.util.List;

import se.frostbyte.jenga.ast.AstNode;
import se.frostbyte.jenga.ast.expressions.BinaryExpression;
import se.frostbyte.jenga.ast.expressions.Expression;
import se.frostbyte.jenga.ast.expressions.VariableExpression;
import se.frostbyte.jenga.ast.statements.BlockStatement;
import se.frostbyte.jenga.ast.statements.DeclarationStatement;
import se.frostbyte.jenga.ast.statements.ForStatement;
import se.frostbyte.jenga.ast.statements.IfStatement;
import se.frostbyte.jenga.ast.statements.WhileStatement;
import se.frostbyte.jenga.parsing.lexer.CToken;
import se.frostbyte.jenga.parsing.lexer.Lexemes;
import se.frostbyte.jenga.parsing.lexer.Lexer;

public class StatementParser {

	public static AstNode parseProgram(Lexer<CToken> lexer) {

		List<AstNode> nodes = new LinkedList<AstNode>();

		while (lexer.peek() != null) {
			nodes.add(parseStatement(lexer));
		}

		AstNode[] nodeArr = new AstNode[nodes.size()];
		return new BlockStatement(nodes.toArray(nodeArr));
	}

	public static AstNode parseStatement(Lexer<CToken> lexer) {

		AstNode treeNode = null;
		int lexeme = lexer.peek().getId();

		switch (lexeme) {
		case Lexemes.If:
			treeNode = parseIf(lexer);
			break;
		case Lexemes.For:
			treeNode = parseFor(lexer);
			break;
		case Lexemes.While:
			treeNode = parseWhile(lexer);
			break;
		case Lexemes.Declaration:
			treeNode = parseDeclaration(lexer);
			break;
		case Lexemes.LBrace:
			treeNode = parseBlock(lexer);
			break;
		default:
			treeNode = ExpressionParser.parseExpression(lexer);
			lexer.expect(Lexemes.Semicolon);
			break;
		}

		return treeNode;
	}

	public static IfStatement parseIf(Lexer<CToken> lexer) {
		lexer.expect(Lexemes.If);
		lexer.expect(Lexemes.LParen);
		Expression condition = ExpressionParser.parseExpression(lexer);
		lexer.expect(Lexemes.RParen);
		AstNode ifBody = parseStatement(lexer);
		AstNode elseBody = null;
		if (lexer.accept(Lexemes.Else)) {
			elseBody = parseStatement(lexer);
		}
		return new IfStatement(condition, ifBody, elseBody);
	}

	public static ForStatement parseFor(Lexer<CToken> lexer) {
		lexer.expect(Lexemes.For);
		lexer.expect(Lexemes.LParen);
		List<DeclarationStatement> declarations = new LinkedList<>();
		do {
			declarations.add(parseDeclaration(lexer));
		} while (lexer.accept(Lexemes.Comma));

		AstNode[] declarationArr = new AstNode[declarations.size()];
		BlockStatement declarationBlock = new BlockStatement(
				declarations.toArray(declarationArr));
		lexer.expect(Lexemes.Semicolon);
		Expression condition = ExpressionParser.parseExpression(lexer);
		lexer.expect(Lexemes.Semicolon);
		AstNode operations = parseStatement(lexer);
		lexer.expect(Lexemes.RParen);
		AstNode body = parseStatement(lexer);
		return new ForStatement(declarationBlock, condition, operations, body);
	}

	public static WhileStatement parseWhile(Lexer<CToken> lexer) {
		lexer.expect(Lexemes.While);
		lexer.expect(Lexemes.LParen);
		Expression condition = ExpressionParser.parseExpression(lexer);
		lexer.expect(Lexemes.RParen);
		AstNode body = parseStatement(lexer);
		return new WhileStatement(condition, body);
	}

	public static BlockStatement parseBlock(Lexer<CToken> lexer) {
		lexer.expect(Lexemes.LBrace);
		List<AstNode> statements = new LinkedList<>();
		while (!lexer.accept(Lexemes.RBrace)) {
			statements.add(parseStatement(lexer));
		}

		AstNode[] statementArr = new AstNode[statements.size()];
		return new BlockStatement(statements.toArray(statementArr));
	}

	public static DeclarationStatement parseDeclaration(Lexer<CToken> lexer) {
		lexer.expect(Lexemes.Declaration);
		BinaryExpression assignment = (BinaryExpression) ExpressionParser
				.parseExpression(lexer);
		VariableExpression variable = (VariableExpression) assignment.Left;
		Expression initializer = assignment.Right;
		lexer.expect(Lexemes.Semicolon);
		return new DeclarationStatement(variable, initializer);
	}
}
