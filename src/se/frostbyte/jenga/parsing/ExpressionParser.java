package se.frostbyte.jenga.parsing;

import se.frostbyte.jenga.ast.Symbol;
import se.frostbyte.jenga.ast.expressions.ConstantExpression;
import se.frostbyte.jenga.ast.expressions.Expression;
import se.frostbyte.jenga.ast.expressions.VariableExpression;
import se.frostbyte.jenga.parsing.lexer.CToken;
import se.frostbyte.jenga.parsing.lexer.Lexemes;
import se.frostbyte.jenga.parsing.lexer.Lexer;
import se.frostbyte.jenga.parsing.operators.Operator;
import se.frostbyte.jenga.parsing.operators.OperatorDefinitions;

import java.util.Stack;

public class ExpressionParser {

    public static Expression parseExpression(Lexer<CToken> lexer) {
        Stack<Expression> values = new Stack<>();

        parseSubExpression(lexer, values);

        return values.pop();
    }

    private static void parseSubExpression(Lexer<CToken> lexer, Stack<Expression> values) {
        Stack<Operator> operators = new Stack<>();

        boolean wasLastOperator = true;

        while (true) {
            CToken peek = lexer.peek();
            switch (peek.getId()) {
                case Lexemes.Comma:
                case Lexemes.Semicolon:
                case Lexemes.RParen:
                    while (!operators.isEmpty()) {
                        values.push(operators.pop().createExpression(values));
                    }
                    return;
            }

            CToken next = lexer.next();
            switch (next.getId()) {
                case Lexemes.Integer:
                    values.push(new ConstantExpression(next.toInt()));
                     wasLastOperator = false;
                    continue;
                case Lexemes.Identifier:
                    values.push(new VariableExpression(Symbol.Create(next.getAttribute())));
                    wasLastOperator = false;
                    continue;
                case Lexemes.LParen:
                    parseSubExpression(lexer, values);
                    lexer.expect(Lexemes.RParen);
                    wasLastOperator = false;
                    continue;
            }

            // Assume operator ;)

            Operator nextOp;
            if (wasLastOperator) {
                nextOp = Operator.unary(next.getId());
            } else {
                nextOp = Operator.binary(next.getId());
            }

            if (!operators.isEmpty()) {
                OperatorDefinitions.OperatorDefinition nod =  nextOp.definition;
                OperatorDefinitions.OperatorDefinition sod =  operators.peek().definition;

                while (!operators.isEmpty() &&
                          ( (nod.precedence < sod.precedence && sod.leftAssoc) ||
                            (nod.precedence == sod.precedence && !sod.leftAssoc))) {
                    values.push(operators.pop().createExpression(values));

                    if (!operators.isEmpty()) {
                        sod =  operators.peek().definition;
                    }
                }
            }

            operators.push(nextOp);

            wasLastOperator = true;
            continue;
        }
    }


}
