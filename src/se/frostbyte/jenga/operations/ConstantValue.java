package se.frostbyte.jenga.operations;

public class ConstantValue extends InstructionValue {
    private final int _value;

    public ConstantValue(int value) {
        _value = value;
    }

    @Override
    public int value() {
        return _value;
    }
}
