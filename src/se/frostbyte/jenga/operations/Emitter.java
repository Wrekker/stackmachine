package se.frostbyte.jenga.operations;

import se.frostbyte.jenga.vm.VirtualMemory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Emitter {
    private int _labelNumber = 1;
    private HashMap<String, Integer> _labelMappings = new HashMap<>();
    private List<Instruction> _instructions = new ArrayList<>();

    public String makeLabel() {
        return "L_" + _labelNumber++;
    }

    public void lda(int address) { instr(OpCode.LDA, address); }
    public void lda(String label) { instr(OpCode.LDA, label); }
    public void ldc(int constant) { instr(OpCode.LDC, constant); }
    public void ldi() { instr(OpCode.LDI); }
           
    public void sta(int address) { instr(OpCode.STA, address); }
    public void sta(String label) { instr(OpCode.STA, label); }
    public void stc(int constant) { instr(OpCode.STC, constant); }
    public void sti() { instr(OpCode.STI); }
           
    public void add() { instr(OpCode.ADD); }
    public void sub() { instr(OpCode.SUB); }
    public void mul() { instr(OpCode.MUL); }
    public void div() { instr(OpCode.DIV); }
    public void neg() { instr(OpCode.NEG); }

    public void and() { instr(OpCode.AND); }
    public void or() { instr(OpCode.OR); }
    public void xor() { instr(OpCode.XOR); }
    public void cpl() { instr(OpCode.CPL); }

    public void label(String name) { _labelMappings.put(name, _instructions.size()); }
           
    public void bra(int address) { instr(OpCode.BRA, address); }
    public void bra(String label) { instr(OpCode.BRA, label); }

    public void brf(int address) { instr(OpCode.BRF, address); }
    public void brf(String label) { instr(OpCode.BRF, label); }

    public void ceq() { instr(OpCode.CEQ); }
    public void cne() { instr(OpCode.CNE); }
    public void cgt() { instr(OpCode.CGT); }
    public void clt() { instr(OpCode.CLT); }
    public void cge() { instr(OpCode.CGE); }
    public void cle() { instr(OpCode.CLE); }
    public void not() { instr(OpCode.NOT); }


    public void pop() { instr(OpCode.POP); }
    public void dup() { instr(OpCode.DUP); }
    public void swp() { instr(OpCode.SWP); }
           
    public void put() { instr(OpCode.PUT); }
           
    public void hlt() { instr(OpCode.HLT); }
    
    private void instr(int opCode) { instr(opCode, 0); }
    private void instr(int opCode, int value) { instr(opCode, new ConstantValue(value)); }
    private void instr(int opCode, String label) { instr(opCode, new LabelValue(label, _labelMappings));}
    private void instr(int opCode, InstructionValue arg) {
        _instructions.add(new Instruction(opCode, arg));
    }

    public void write(VirtualMemory memory) {
        for (Instruction ins : _instructions) {
            System.out.println(OpCode.Lookup.get(ins.OpCode) + "\t\t" + ins.Value.value());
            memory.instr(ins.OpCode, ins.Value.value());
        }
    }
}

