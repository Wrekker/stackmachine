package se.frostbyte.jenga.operations;

import java.lang.reflect.Field;
import java.util.HashMap;

public class OpCode {
    public static final HashMap<Integer, String> Lookup = new HashMap<>();

    public static final int
            NOP = 0,
            ADD = 2,
            SUB = 3,
            MUL = 4,
            DIV = 5,
            LDA = 6,
            LDC = 7,
            STA = 8,
            STC = 9,

            BRA = 10,
            CEQ = 11,
            CNE = 12,
            PUT = 13,
            POP = 14,
            DUP = 15,

            LDI = 16,
            STI = 17,
            BRF = 18,

            CGT = 19,
            CLT = 20,
            CGE = 21,
            CLE = 22,

            SWP = 23,

            CPL = 24,
            AND = 25,
            OR = 26,

            NEG = 27,
            XOR = 28,
            NOT = 29,

            HLT = 255
            ;

    static {
        for (Field f : OpCode.class.getFields()) {
            if (f.getType() != int.class) continue;

            try {
                Lookup.put(f.getInt(OpCode.class), f.getName());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }


}
