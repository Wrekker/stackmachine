package se.frostbyte.jenga.operations;

import java.util.HashMap;
import java.util.Map;

public class LabelValue extends InstructionValue {
    private final String _name;
    private final Map<String, Integer> _mappings;

    public LabelValue(String name, HashMap<String, Integer> mappings) {
        _name = name;
        _mappings = mappings;
    }

    @Override
    public int value() {
        return _mappings.get(_name);
    }
}
