package se.frostbyte.jenga.operations;

public abstract class InstructionValue {
    public abstract int value();
}
