package se.frostbyte.jenga.operations;

public class Instruction {
    public final int OpCode;
    public InstructionValue Value;

    public Instruction(int opCode, InstructionValue value) {
        OpCode = opCode;
        Value = value;
    }
}
