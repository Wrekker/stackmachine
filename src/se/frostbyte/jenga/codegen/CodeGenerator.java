package se.frostbyte.jenga.codegen;

import se.frostbyte.jenga.ast.AstNode;
import se.frostbyte.jenga.ast.NodeType;
import se.frostbyte.jenga.ast.expressions.*;
import se.frostbyte.jenga.ast.statements.*;
import se.frostbyte.jenga.operations.Emitter;
import se.frostbyte.jenga.vm.VirtualMemory;

public class CodeGenerator {
    Emitter _ow;
    VirtualMemory _mem;

    public CodeGenerator(Emitter ow, VirtualMemory mem) {
        _ow = ow;
        _mem = mem;
    }

    public void generate(AstNode node) {
        generateAny(node);
        _ow.hlt();
    }

    private void generateAny(AstNode node) {
        if (node instanceof ScalarExpression) {
            generateScalar((ScalarExpression) node);
        } else if (node instanceof UnaryExpression) {
            generateUnary((UnaryExpression)node);
        } else if (node instanceof BinaryExpression) {
            generateBinary((BinaryExpression) node);
        } else {
            generateStatement(node);
        }
    }

    private void generateUnary(UnaryExpression node) {
        generateAny(node.value);

        switch (node.type) {
            case NodeType.NotExpression: _ow.not(); break;
            case NodeType.NegateExpression: _ow.neg(); break;
        }
    }

    private void generateBinary(BinaryExpression node) {
        generateAny(node.left);
        generateAny(node.right);

        switch (node.type) {
            case NodeType.AddExpression: _ow.add(); break;
            case NodeType.SubtractExpression: _ow.sub(); break;
            case NodeType.MultiplyExpression: _ow.mul(); break;
            case NodeType.DivideExpression: _ow.div(); break;
            case NodeType.EqualityExpression: _ow.ceq(); break;
            case NodeType.InequalityExpression: _ow.cne(); break;
            case NodeType.GreaterExpression: _ow.cgt(); break;
            case NodeType.LesserExpression: _ow.clt(); break;
            case NodeType.GreaterEqualExpression: _ow.cge(); break;
            case NodeType.LesserEqualExpression: _ow.cle(); break;
            case NodeType.AssignExpression: _ow.sti(); break;         //TODO This is wrongo!
        }
    }

    private void generateStatement(AstNode node) {
        switch (node.type) {
            case NodeType.WhileStatement: generateWhile((WhileStatement) node); break;
            case NodeType.AssignStatement: generateAssign((AssignStatement) node); break;
            case NodeType.DeclarationStatement: generateDeclare((DeclarationStatement) node); break;
            case NodeType.BlockStatement: generateBlock((BlockStatement) node); break;
            case NodeType.IfStatement: generateIf((IfStatement) node); break;
        }
    }

    private void generateIf(IfStatement node) {
        String altLabel = _ow.makeLabel();
        String endLabel = _ow.makeLabel();

        generateAny(node.condition);
        _ow.brf(altLabel);
        generateAny(node.consequent);
        _ow.bra(endLabel);
        _ow.label(altLabel);
        generateAny(node.alternative);
        _ow.label(endLabel);
    }

    private void generateBlock(BlockStatement node) {
        for (AstNode stmt : node.nodes) {
            generateAny(stmt);
        }
    }

    private void generateDeclare(DeclarationStatement node) {
        try {
            int adr = _mem.alloc(node.variable.Symbol);

            generateAny(node.initializer);
            _ow.sta(adr);
        } catch (VirtualMemory.MemoryAllocationException e) {
            e.printStackTrace();
        }
    }

    private void generateAssign(AssignStatement node) {
        generateAny(node.right);
        int adr = 0;
        try {
            adr = _mem.lookup(node.left.Symbol);
        } catch (VirtualMemory.MemoryAllocationException e) {
            e.printStackTrace();
        }
        _ow.sta(adr);
    }

    private void generateWhile(WhileStatement node) {
        String startLabel = _ow.makeLabel();
        String endLabel = _ow.makeLabel();

        _ow.label(startLabel);
        generateAny(node.condition);
        _ow.brf(endLabel);
        generateAny(node.body);
        _ow.bra(startLabel);
        _ow.label(endLabel);
    }

    private void generateScalar(ScalarExpression node) {

        switch (node.type) {
            case NodeType.ConstantExpression:
                _ow.ldc(((ConstantExpression)node).value);
                break;
            case NodeType.VariableExpression:
                try {
                    _ow.lda(_mem.lookup(((VariableExpression)node).Symbol));
                } catch (VirtualMemory.MemoryAllocationException e) {
                    e.printStackTrace();
                }
                break;
        }
    }



}
