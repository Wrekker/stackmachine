package se.frostbyte.jenga.vm;

import se.frostbyte.jenga.ast.Symbol;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VirtualMemory {

	public static final int PROG_MEM_CAP = 64;
	public static final int PROG_MEM_MASK = PROG_MEM_CAP - 1;

	private int _mp = 0;
	private int _sp = PROG_MEM_MASK;
	private int[] _mem = new int[PROG_MEM_CAP];

	private Map<Symbol, MemoryAllocation> _allocMap = new HashMap<>(10);
	private List<Integer> _freeList = new ArrayList<>();

	/**
	 * Push element to program space. Pushing too many elements may cause
	 * overwriting of instructions. /** Add instruction with value
	 * 
	 * @param opCode
	 * @param value
	 * @return address
	 */
	public int instr(int opCode, int value) {
		_mem[_mp] = (value << 0x08) | (opCode & 0xff);
		return _mp++;
	}

	public int load(int adr) {
		return _mem[adr];
	}

	public void save(int adr, int value) {
		_mem[adr] = value;
	}

	/**
	 * Push element to program space. Pushing too many elements may cause
	 * overwriting of instructions.
	 * 
	 * @param value
	 */
	public void push(int value) {
		_mem[_sp-- & PROG_MEM_MASK] = value;
	}

	/**
	 * Pops element from program space. Wraps aruond if stack pointer exceeds
	 * vm space. If stack pointer wraps around the stack will overwrite
	 * instructions.being()
	 * 
	 * @return element
	 */
	public int pop() {
		return _mem[++_sp & PROG_MEM_MASK];
	}

	// Not to be used at this moment
	// public int peek(int n) { return _mem[_sp + n]; }

	/**
	 * Allocated vm, if any allocated vm is marked as deallocated that
	 * space is used.
	 * 
	 * @param var
	 * @return address
	 * @throws MemoryAllocationException
	 */
	public int alloc(Symbol var) throws MemoryAllocationException {
		if (!_allocMap.containsKey(var)) {
			int _allocAdr = _freeList.isEmpty() ? _sp-- : _freeList.get(0);
			_allocMap.put(var, new MemoryAllocation(_allocAdr));
			return _allocAdr;
		} else {
			MemoryAllocation alloc;
			if ((alloc = _allocMap.get(var)).marked) {
				alloc.marked = false;
				return alloc.adr;
			} else
				throw new MemoryAllocationException(new Throwable(
						"Expecting free variable identifier: " + var));

		}

	}

	/**
	 * Deallocates vm, if the allocation is marked as remove the address is
	 * added to {@link #_freeList the list of free addresses } so that space can
	 * be reused.
	 * 
	 * @param var
	 * @throws MemoryAllocationException
	 */
	public void dealloc(Symbol var) throws MemoryAllocationException {
		if (_allocMap.containsKey(var)) {
			MemoryAllocation alloc;
			if (!(alloc = _allocMap.get(var)).marked) {
				alloc.marked = true;
				_freeList.add(alloc.adr);
			}
		} else
			throw new MemoryAllocationException(new Throwable(
					"Cannot deallocate non-allocated memory. "));
	}

	/**
	 * Looks up an allocated resource
	 * 
	 * @param var
	 * @return address if any, otherwise -1
	 * @throws MemoryAllocationException 
	 */
	public int lookup(Symbol var) throws MemoryAllocationException {
		if (_allocMap.containsKey(var)){
			return _allocMap.get(var).adr;
		} else throw new MemoryAllocationException("Unknown identifier. ");
	}

	/**
	 * Holds the allocated vm address, can be marked as removed.
	 */
	class MemoryAllocation {
		private boolean marked = false;
		private int adr;

		public MemoryAllocation(int adr) {
			this.adr = adr;
		}

		public void setMarked(boolean marked) {
			this.marked = marked;
		}
	}

	public class MemoryAllocationException extends Exception {
		private static final long serialVersionUID = 1083615540889813922L;

		public MemoryAllocationException() {
			this(null, null);
		}

		public MemoryAllocationException(String message) {
			this(message, null);
		}

		public MemoryAllocationException(Throwable cause) {
			this(null, cause);
		}

		public MemoryAllocationException(String message, Throwable cause) {
			super(message, cause);
		}
	}
}
