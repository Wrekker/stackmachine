package se.frostbyte.jenga.vm;

import se.frostbyte.jenga.operations.OpCode;

import java.util.Stack;

public class Interpreter {
    public final int TRUE = -1;
    public final int FALSE = 0;

    //private SymbolTable _symbolTable = new SymbolTable();
    private Stack<Integer> _returnStack = new Stack<>();
    private Stack<Integer> _operandStack = new Stack<>();
    private int[] _memory = new int[256];
    
    public int execute() {
        int pc = 0;

        while (true) {
            int instr = _memory[pc];

            int opc = (instr & 0xff);
            int arg = instr >> 8;
            int temp, temp2;

            pc++;
            
            switch (opc) {
                case OpCode.NOP:
                    break;
                case OpCode.LDA:
                    _operandStack.push(_memory[arg]);
                    break;
                case OpCode.LDC:
                    _operandStack.push(arg);
                    break;
                case OpCode.LDI:
                    _operandStack.push(_memory[_operandStack.pop()]);
                    break;
                case OpCode.STA:
                    _memory[arg] = _operandStack.pop();
                    break;
                case OpCode.STC:
                    _memory[_operandStack.pop()] = arg;
                    break;
                case OpCode.STI:
                    _memory[_operandStack.pop()] = _operandStack.pop();
                    break;
                case OpCode.ADD:
                    _operandStack.push(_operandStack.pop() + _operandStack.pop());
                    break;
                case OpCode.SUB:
                    temp = _operandStack.pop();
                    _operandStack.push(_operandStack.pop() - temp);
                    break;
                case OpCode.MUL:
                    _operandStack.push(_operandStack.pop() * _operandStack.pop());
                    break;
                case OpCode.DIV:
                    temp = _operandStack.pop();
                    _operandStack.push(_operandStack.pop() / temp);
                    break;
                case OpCode.NEG:
                    _operandStack.push(-_operandStack.pop());

                case OpCode.BRA:
                    pc = arg;
                    break;

                case OpCode.BRF:
                    pc = (_operandStack.pop() == FALSE) ? arg : pc;
                    break;

                case OpCode.CEQ:
                    _operandStack.push(_operandStack.pop() == _operandStack.pop() ? TRUE : FALSE);
                    break;
                case OpCode.CNE:
                    _operandStack.push(_operandStack.pop() != _operandStack.pop() ? TRUE : FALSE);
                    break;
                case OpCode.CGT:
                    temp = _operandStack.pop();
                    _operandStack.push(_operandStack.pop() > temp ? TRUE : FALSE);
                    break;
                case OpCode.CLT:
                    temp = _operandStack.pop();
                    _operandStack.push(_operandStack.pop() < temp ? TRUE : FALSE);
                    break;
                case OpCode.CGE:
                    temp = _operandStack.pop();
                    _operandStack.push(_operandStack.pop() >= temp ? TRUE : FALSE);
                    break;
                case OpCode.CLE:
                    temp = _operandStack.pop();
                    _operandStack.push(_operandStack.pop() <= temp ? TRUE : FALSE);
                    break;
                
                case OpCode.AND:
                    _operandStack.push(_operandStack.pop() & _operandStack.pop());
                    break;
                case OpCode.OR:
                    _operandStack.push(_operandStack.pop() | _operandStack.pop());
                    break;
                case OpCode.XOR:
                    _operandStack.push((_operandStack.pop() ^ _operandStack.pop()));
                    break;
                case OpCode.CPL:
                    _operandStack.push(~_operandStack.pop());
                    break;

                case OpCode.POP:
                    _operandStack.pop();
                    break;
                case OpCode.DUP:
                    temp = _operandStack.pop();
                    _operandStack.push(temp);
                    _operandStack.push(temp);
                    break;
                case OpCode.SWP:
                    temp = _operandStack.pop();
                    temp2 = _operandStack.pop();
                    _operandStack.push(temp2);
                    _operandStack.push(temp);
                case OpCode.PUT:
                    System.out.println(_operandStack.pop());
                    break;
                case OpCode.HLT:
                    return 0;
            }
        }
    }



}
