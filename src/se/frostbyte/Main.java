package se.frostbyte;

import se.frostbyte.jenga.ast.AstNode;
import se.frostbyte.jenga.ast.Symbol;
import se.frostbyte.jenga.ast.expressions.AddExpression;
import se.frostbyte.jenga.ast.expressions.ConstantExpression;
import se.frostbyte.jenga.ast.expressions.InequalityExpression;
import se.frostbyte.jenga.ast.expressions.SubtractExpression;
import se.frostbyte.jenga.ast.expressions.VariableExpression;
import se.frostbyte.jenga.ast.statements.AssignStatement;
import se.frostbyte.jenga.ast.statements.BlockStatement;
import se.frostbyte.jenga.ast.statements.DeclarationStatement;
import se.frostbyte.jenga.ast.statements.WhileStatement;
import se.frostbyte.jenga.codegen.CodeGenerator;
import se.frostbyte.jenga.operations.Emitter;
import se.frostbyte.jenga.vm.Interpreter;
import se.frostbyte.jenga.vm.VirtualMemory;

public class Main {
    public static void main(String[] args) {
        VirtualMemory memory = new VirtualMemory();
        Emitter emitter = new Emitter();
        CodeGenerator gen = new CodeGenerator(emitter, memory);
        Interpreter m = new Interpreter();

        Symbol q = Symbol.Create("Q");

        AstNode ast =
                new BlockStatement(
                    new DeclarationStatement(new VariableExpression(q), new AddExpression(new ConstantExpression(10), new ConstantExpression(10))),
                    new WhileStatement(
                            new InequalityExpression(
                                    new VariableExpression(q),
                                    new ConstantExpression(9)),
                            new BlockStatement(
                                    new AssignStatement(
                                            new VariableExpression(q),
                                            new SubtractExpression(
                                                    new VariableExpression(q),
                                                    new ConstantExpression(1))))));
        // for (int i=0;i<10;i++)
        // for ( int i = 0 ; i < 10 ; i ++ )

        // var q = 10
        //


        gen.generate(ast);
        emitter.write(memory);

        m.execute(memory);

        System.out.println("urk");
    }
}
